from matplotlib import pyplot as plt
import numpy as np
import csv, time, random, math



#Settings
filename = "output__std.csv"
output_filename = 'output.svg'

title = "population over time"
xlabel = 'Time'
ylabel = 'Population'
grid = True
max_ticks = 10000




def read():
    global times, values1, values2, values3
    with open(filename, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')

        for row in reader:
            #print(row)
            if row[0] == "tick":
                print(row)
            else:
                times.append(int(row[0]))
                values1.append(int(row[1]))
                values2.append(int(row[2]))
                values3.append(int(row[3]))

plt.figure(figsize=[10, 7])

times = []
values1 = []
values2 = []
values3 = []

read()


#for i in range(600):
#    times.append(i)
#    values1.append(math.sin((2*math.pi*i/300)))
#    values2.append((math.sin((2*math.pi*(i/300)+math.pi/2)))*1.2+0.4)



#plt.plot(times[:],values1[:], label='predator')
#plt.plot(times[:],values3[:], '#00ff00', label='Herbivore')
#plt.plot(times[:],values2[:], label='prey')



plt.plot(times[:],values1[:], '#ff0000', label='Carnivore')
plt.plot(times[:],values3[:], '#00ff00', label='Herbivore')
plt.plot(times[:],values2[:], '#ffbe40', label='Food')


plt.ylim(0,200)


plt.legend(loc='upper right')
plt.grid(grid)
plt.xlabel(xlabel)
plt.ylabel(ylabel)
plt.title(title)

#plt.xticks([  ])
#plt.yticks([  ])

plt.xticks(np.arange(min(times), max(times)+1, 1000))
#plt.xticks(ticks)#np.arange(min(times), max(times)+1, int(max_ticks/100)))

plt.xlim(0,max_ticks)

plt.savefig(output_filename)

plt.show()
