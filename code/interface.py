import pygame
from button import *
import settings
from mathematics import bool_check
import math
from profiler import profile
import csv
import time
import getpass

"""

    This class is for the user interface. It enables the user to interact with the simulation
    and vary parameters of carnivores, herbivores and food.

"""

class Interface:

    """

    Initializing the width and height of the display. The default display size is set to the 16:9 format.
    This insures that opening the simulation doesn't cause any problems, due to wrong formating. The windowsize of
    the simulation can also be set manually.
    This Method also initializes the buttons for the userinterface and sets the bool variables for submenues.

    """

    def __init__(self):

        #initializing screen and main screen and a Surface to draw on
        self.screen = pygame.display.set_mode((settings.width,settings.height), pygame.RESIZABLE)
        self.mainScreen = pygame.Surface((settings.width,settings.height))

        #Bool variable that is used in the case that the display size is modifiyed
        self.size_changed = False

        self.settings_filename = 'settings.csv'

        # Variable to display fps
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 30)

        #Variable for the time the Message is displayed in the messagebox
        self.msg_time = 0

        #Bool variabeles for displaying multiple menu layers
        self.startbuttonstate = False
        self.resetbuttonstate = False
        self.plotbuttonstate = False
        self.herbSettingsState = False
        self.carnSettingsState = False
        self.foodSettingsState = False

        # Initializing the buttons for the Menu
        self.btn_start_stop = button((0,0,0), settings.width*8/200, 10, 150, 30,'START')
        self.btn_reset = button((0,0,0), settings.width*38/200, 10, 150, 30, 'RESET')
        self.btn_hideouts = button((0,0,0), settings.width*68/200, 10, 150, 30, 'Hideouts ON')
        self.btn_carn_settings = button((0,0,0), settings.width*8/200, 60, 150, 30, 'Carnivore Settings')
        self.btn_herb_settings = button((0,0,0), settings.width*38/200, 60, 150, 30, 'Herbivore Settings')
        self.btn_bait_settings = button((0,0,0), settings.width*68/200, 60, 150, 30, 'Food Settings')
        self.btn_back = button((0,0,0), settings.width*8/200, 10, 150, 30,'Back')

        self.btn_plot = button((0,0,0), settings.width*8/200, 160, 150, 30, 'Plot 1')
        self.btn_csv_output = button((0,0,0), settings.width*38/200, 160, 150, 30,'CSV Output')
        self.btn_export_settings = button((0,0,0), settings.width*8/200, 110, 150, 30,'Export Settings')
        self.btn_import_settings = button((0,0,0), settings.width*38/200, 110, 150, 30,'Import Settings')

        self.btn_carn_state = button((0,0,0), settings.width*38/200, 10, 150, 30,'Carnivore OFF')
        self.btn_herb_state = button((0,0,0), settings.width*38/200, 10, 150, 30,'Herbivore OFF')
        self.btn_bait_state = button((0,0,0), settings.width*38/200, 10, 150, 30,'Food OFF')

        if settings.bait_state == False:
            self.btn_bait_state.text = "Food ON"
        else:
            self.btn_bait_state.text = "Food OFF"

        if settings.carnivore_state == False:
            self.btn_carn_state.text = "Carnivore ON"
        else:
            self.btn_carn_state.text = "Carnivore OFF"

        if settings.herbivore_state == False:
            self.btn_herb_state.text = "Herbivore ON"
        else:
            self.btn_herb_state.text = "Herbivore OFF"

        if settings.hideout_state == False:
            self.btn_hideouts.text = "Hideouts ON"
        else:
            self.btn_hideouts.text = "Hideouts OFF"



        self.msg_box = rectangle_text(settings.color, settings.width*38/200-175, 210, 500, 30,'') #for displaying Messages

        # Arrow buttons to change values in the subsettings menu
        self.upButton1 = Triangle_Button((0,0,0), settings.width*80/200-15, 50, 30, 15, 0,1)
        self.downButton1 = Triangle_Button((0,0,0),settings.width*80/200+95, 80, 30, 15, 0,0)

        self.upButton2 = Triangle_Button((0,0,0), settings.width*80/200-15, 100, 30, 15, 0,1)
        self.downButton2 = Triangle_Button((0,0,0),settings.width*80/200+95, 130, 30, 15, 0,0)

        self.upButton3 = Triangle_Button((0,0,0), settings.width*80/200-15, 150, 30, 15, 0,1)
        self.downButton3 = Triangle_Button((0,0,0),settings.width*80/200+95, 180, 30, 15, 0,0)

        self.upButton4 = Triangle_Button((0, 0, 0), settings.width *80/200-15, 200, 30, 15, 0, 1)
        self.downButton4 = Triangle_Button((0, 0, 0), settings.width *80/200+95, 230, 30, 15, 0, 0)

        #textboxes (tb) to display varriables in settings menu

        self.tb_variable_1 = rectangle_text((0,0,0), settings.width*80/200, 50, 80, 30, 'Variable')
        self.tb_variable_2 = rectangle_text((0,0,0), settings.width*80/200, 100, 80, 30, 'Variable')
        self.tb_variable_3 = rectangle_text((0,0,0), settings.width*80/200, 150, 80, 30, 'Variable')
        self.tb_variable_4 = rectangle_text((0,0,0), settings.width*80/200, 200, 80, 30, 'Variable')

        #textboxes (tb) for labeling (lbl) text boxes with Units (u)

        self.tb_lbl_start_value = rectangle_text(settings.color, settings.width * 80/200-136, 50, 80, 30, 'Start Value:', (0,0,0))
        self.lbl_start_value_u_1 = rectangle_text(settings.color, settings.width * 80/200-136, 75, 80, 15, '[# of Animals]', (0,0,0))
        self.lbl_start_value_u_2 = rectangle_text(settings.color, settings.width * 80/200-136, 75, 80, 15, '[# of Food]', (0,0,0))

        self.lbl_lifespan = rectangle_text(settings.color, settings.width * 80/200-136, 100, 80, 30, 'Lifespan:', (0,0,0))
        self.lbl_lifespan_u_1 = rectangle_text(settings.color, settings.width * 80/200-136, 125, 80, 15, '[ticks]',(0,0,0))

        self.lbl_velocity = rectangle_text(settings.color, settings.width * 80/200-136, 150, 80, 30, 'Velocity:', (0,0,0))
        self.lbl_velocity_u_1 = rectangle_text(settings.color, settings.width * 80/200-136, 175, 80, 15, '[m/tick]', (0,0,0))

        self.lbl_reproduction = rectangle_text(settings.color, settings.width * 80/200-136, 200, 80, 30, 'Reproduction:', (0,0,0))
        self.lbl_reproduction_u_1 = rectangle_text(settings.color, settings.width * 80/200-136, 225, 80, 15, '[food/reproduction]', (0,0,0))

        self.lbl_max_amount = rectangle_text(settings.color, settings.width * 80/200-136, 100, 80, 30, 'Maximum Amount :', (0,0,0))
        self.lbl_max_amount_u_1 = rectangle_text(settings.color, settings.width * 80/200-136, 125, 80, 15, '[# of Food]', (0,0,0))

        self.lbl_regeneration = rectangle_text(settings.color, settings.width * 80/200-136, 150, 80, 30, 'Regeneration:', (0,0,0))
        self.lbl_regeneration_u_1 = rectangle_text(settings.color, settings.width * 80/200-136, 175, 80, 15, 'f(max. Amount) [%]', (0,0,0))

        pygame.init()

    """
        This method refreshes the Surfaces if the windowsize of the Simulation is changed.
        This method also refreshes the messagebox in the mainmenu to display messages (for a set timeperiod)
        for user input confirmation.

    """
    def refresh_surfaces(self):



        self.mainScreen.set_colorkey((0,0,0))

        self.mainScreen = pygame.Surface((settings.width,settings.height))

        animationSurface = pygame.Rect(0,0,settings.width/2, settings.height)
        interfaceSurface = pygame.Rect(settings.width/2,0,settings.width/2,settings.height/3)
        plotSurface = pygame.Rect(settings.width/2 , settings.height/3, settings.width/2, (2*settings.height)/3)

        # Defines Subscreens of the Mainscreen and the subscreen background color
        self.subSurface1 = self.mainScreen.subsurface(animationSurface)
        self.subSurface1.fill((0,0,0))
        self.subSurface2 = self.mainScreen.subsurface(interfaceSurface)
        self.subSurface2.fill(settings.color)
        pygame.draw.line(self.subSurface2, (0, 0, 0), (0, self.subSurface2.get_height()-2), (self.subSurface2.get_width(), self.subSurface2.get_height()-2))
        self.subSurface3 = self.mainScreen.subsurface(plotSurface)
        self.subSurface3.fill((255,255,255))


        # refresh message box
        if not settings.msg_text == "":
            if self.msg_time == 0:
                self.msg_time = time.time()
                self.msg_box.text_color = (0,0,0)
                print("displaying message:", settings.msg_text)
            if time.time() - self.msg_time >= 5:
                self.msg_time = 0
                settings.msg_text = ""
        self.msg_box.text = settings.msg_text

    """

        This method updates the screens, to enable dynamic operation. This method enables the user to display the
        current frames per second on the simulation surface. Turning the fps display off and on is done with the bool variable
        fps_state in settings.py

    """

    def update_surfaces(self):
        if settings.fps_state:
            fps = self.font.render(str(int(self.clock.get_fps())), True, (255, 255, 255))
            self.subSurface1.blit(fps, (10, 10))

        #if settings.game_state == True:
        self.screen.blit(self.subSurface2, (settings.width/2, 0))
        self.screen.blit(self.subSurface1, (0,0))


        pygame.display.flip()
        #pygame.display.update()

        self.clock.tick(40)

    """

        This method enables only drawing the desired buttons on the screens, depending on in which menu
        the user is in. This is necessary to create a Mainmenu wuth submenues. Depending on the state of the
        bool variables other buttons are displayed and enabled for user interface

    """

    #@profile
    def refresh_buttons(self):
        #At the begining all Buttons are displayed if none of the bool variables are true

        if  self.herbSettingsState == False and self.carnSettingsState == False and self.foodSettingsState == False:  # if start Button is pressed buttons to change Values change
            self.btn_start_stop.draw(self.subSurface2, (0, 0, 0))
            self.btn_reset.draw(self.subSurface2, (0, 0, 0))
            self.btn_hideouts.draw(self.subSurface2, (0, 0, 0))
            self.btn_carn_settings.draw(self.subSurface2, (0, 0, 0))
            self.btn_herb_settings.draw(self.subSurface2, (0, 0, 0))
            self.btn_bait_settings.draw(self.subSurface2, (0, 0, 0))
            self.btn_plot.draw(self.subSurface2, (0, 0, 0))
            self.btn_csv_output.draw(self.subSurface2, (0, 0, 0))
            self.btn_export_settings.draw(self.subSurface2, (0, 0, 0))
            self.btn_import_settings.draw(self.subSurface2, (0, 0, 0))
            self.msg_box.draw(self.subSurface2)

        #submenue for the Herbivores
        if self.herbSettingsState == True:
            self.btn_back.draw(self.subSurface2, (0, 0, 0))
            self.btn_herb_state.draw(self.subSurface2, (0, 0, 0))


            animal_center_x = int(settings.width * 8 / 200 +90) + int(math.sin(pygame.time.get_ticks() * math.pi / 2000) * 50)
            animal_center_y = int(settings.height*15/200+90) + int(math.cos(pygame.time.get_ticks() * math.pi / 2000) * 50)


            pygame.draw.rect(self.subSurface2, (0,0,0), (settings.width*8/200, settings.height*15/200, 180, 180), 2)

            pygame.gfxdraw.aacircle(self.subSurface2, animal_center_x, animal_center_y, 15, (0, 255, 0))

            pygame.gfxdraw.filled_circle(self.subSurface2,animal_center_x, animal_center_y, 15, (0, 255, 0))


            pygame.draw.line(self.subSurface2, (0, 255, 0),(animal_center_x,animal_center_y),
            (animal_center_x + int(math.sin(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 60),
             animal_center_y + int(math.cos(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 60)), 6)

            self.tb_variable_1.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_2.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_3.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_4.draw(self.subSurface2, (0, 0, 0))

            self.tb_lbl_start_value.draw(self.subSurface2, settings.color)
            self.lbl_start_value_u_1.draw(self.subSurface2, settings.color)

            self.lbl_lifespan.draw(self.subSurface2, settings.color)
            self.lbl_lifespan_u_1.draw(self.subSurface2, settings.color)

            self.lbl_velocity.draw(self.subSurface2, settings.color)
            self.lbl_velocity_u_1.draw(self.subSurface2, settings.color)

            self.lbl_reproduction.draw(self.subSurface2, settings.color)
            self.lbl_reproduction_u_1.draw(self.subSurface2, settings.color)


            self.upButton1.draw(self.subSurface2)
            self.downButton1.draw(self.subSurface2)
            self.upButton2.draw(self.subSurface2)
            self.downButton2.draw(self.subSurface2)
            self.upButton3.draw(self.subSurface2)
            self.downButton3.draw(self.subSurface2)
            self.upButton4.draw(self.subSurface2)
            self.downButton4.draw(self.subSurface2)

        #SUbmenu for the Carnivorees
        if self.carnSettingsState == True:
            self.btn_back.draw(self.subSurface2, (0, 0, 0))
            self.btn_carn_state.draw(self.subSurface2, (0, 0, 0))

            animal_center_x = int(settings.width * 8 / 200 + 90) + int(math.sin(pygame.time.get_ticks() * math.pi / 2000) * 50)
            animal_center_y = int(settings.height * 15 / 200 + 90) + int(math.cos(pygame.time.get_ticks() * math.pi / 2000) * 50)

            pygame.draw.rect(self.subSurface2, (0, 0, 0),(settings.width * 8 / 200, settings.height * 15 / 200, 180, 180), 2)

            pygame.gfxdraw.aacircle(self.subSurface2, animal_center_x, animal_center_y, 15, (255, 0, 0))

            pygame.gfxdraw.filled_circle(self.subSurface2, animal_center_x, animal_center_y, 15, (255, 0, 0))

            pygame.gfxdraw.aacircle(self.subSurface2,
            animal_center_x + int(math.sin(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 30),
            animal_center_y + int(math.cos(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 30),
            15, (255, 0, 0))

            pygame.gfxdraw.filled_circle(self.subSurface2,
            animal_center_x + int(math.sin(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 30),
            animal_center_y + int(math.cos(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 30),
            15, (255, 0, 0))

            pygame.draw.line(self.subSurface2, (255, 0, 0), (animal_center_x, animal_center_y),
            (animal_center_x + int(math.sin(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 60),
             animal_center_y + int(math.cos(pygame.time.get_ticks() * math.pi / 2000 + 0.5 * math.pi) * 60)), 6)


            self.tb_variable_1.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_2.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_3.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_4.draw(self.subSurface2, (0, 0, 0))

            self.tb_lbl_start_value.draw(self.subSurface2, settings.color)
            self.lbl_start_value_u_1.draw(self.subSurface2, settings.color)

            self.lbl_lifespan.draw(self.subSurface2, settings.color)
            self.lbl_lifespan_u_1.draw(self.subSurface2, settings.color)

            self.lbl_velocity.draw(self.subSurface2, settings.color)
            self.lbl_velocity_u_1.draw(self.subSurface2, settings.color)

            self.lbl_reproduction.draw(self.subSurface2, settings.color)
            self.lbl_reproduction_u_1.draw(self.subSurface2, settings.color)

            self.upButton1.draw(self.subSurface2)
            self.downButton1.draw(self.subSurface2)
            self.upButton2.draw(self.subSurface2)
            self.downButton2.draw(self.subSurface2)
            self.upButton3.draw(self.subSurface2)
            self.downButton3.draw(self.subSurface2)
            self.upButton4.draw(self.subSurface2)
            self.downButton4.draw(self.subSurface2)


        #Submenu for the bait
        if self.foodSettingsState == True:

            pygame.draw.rect(self.subSurface2, (0,0,0), (settings.width*8/200, settings.height*15/200, 180, 180), 2)
            pygame.gfxdraw.aacircle(self.subSurface2, int(settings.width*8/200+90), int(settings.height*15/200+90), 15, (255,190,65))
            pygame.gfxdraw.filled_circle(self.subSurface2, int(settings.width*8/200+90), int(settings.height*15/200+90), 15, (255,190,65))


            self.btn_back.draw(self.subSurface2, (0, 0, 0))
            self.btn_bait_state.draw(self.subSurface2, (0, 0, 0))


            self.tb_variable_1.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_2.draw(self.subSurface2, (0, 0, 0))
            self.tb_variable_3.draw(self.subSurface2, (0, 0, 0))

            self.tb_lbl_start_value.draw(self.subSurface2, settings.color)
            self.lbl_start_value_u_2.draw(self.subSurface2, settings.color)

            self.lbl_max_amount.draw(self.subSurface2, settings.color)
            self.lbl_max_amount_u_1.draw(self.subSurface2, settings.color)

            self.lbl_regeneration.draw(self.subSurface2, settings.color)
            self.lbl_regeneration_u_1.draw(self.subSurface2, settings.color)

            self.upButton1.draw(self.subSurface2)
            self.downButton1.draw(self.subSurface2)
            self.upButton2.draw(self.subSurface2)
            self.downButton2.draw(self.subSurface2)
            self.upButton3.draw(self.subSurface2)
            self.downButton3.draw(self.subSurface2)

    """
        This method checks for events in the pygame queue. It checks if the program was quited. In that case
        the global variable running is set to False and the program is ended.
        The event type VIDEORESIZE checks if the window was scaled. If so, the width and height of the window
        is set to the new values. Furthermore the mouseposition and the state of the right mousebutton is checked to
        enable the shading of the buttons when the mouse hovers over and enable the selection of desired buttons.

    """


    def check_events(self, event):
        global running
        self.pos = pygame.mouse.get_pos()
        if event.type == pygame.QUIT:
                running = False
                quit()

        # Issues with return of pygame.VIDEORESIZE on macOS 10.15.2 (19C57)
        if not getpass.getuser() == "dominik" and event.type == pygame.VIDEORESIZE:
            settings.width, settings.height = event.dict['size']
            if settings.width <= 1040:
                settings.width = 1040
            if settings.height <= 720:
                settings.height = 720

            self.resize_buttons()

            pygame.display.flip()

            self.size_changed = True

        else:
             self.size_changed = False



        #If the mouseposition coincides with the position of the button, the button switches to the selected color

        if event.type == pygame.MOUSEMOTION:
            if self.pos[0] >= (settings.width/2) and self.pos[1] <= (settings.height/3):
                self.btn_start_stop.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_reset.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_hideouts.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_carn_settings.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_herb_settings.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_bait_settings.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_plot.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_back.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_csv_output.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_export_settings.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_import_settings.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_carn_state.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_herb_state.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))
                self.btn_bait_state.isOver(self.pos, (settings.width / 2), 0, (133, 133, 133), (0, 0, 0))



                self.upButton1.isOver(self.pos, (settings.width / 2), 0, (0, 200, 0), (0, 0, 0))
                self.downButton1.isOver(self.pos, (settings.width / 2), 0, (200, 0, 0), (0, 0, 0))

                self.upButton2.isOver(self.pos, (settings.width / 2), 0, (0, 200, 0), (0, 0, 0))
                self.downButton2.isOver(self.pos, (settings.width / 2), 0, (200, 0, 0), (0, 0, 0))

                self.upButton3.isOver(self.pos, (settings.width / 2), 0, (0, 200, 0), (0, 0, 0))
                self.downButton3.isOver(self.pos, (settings.width / 2), 0, (200, 0, 0), (0, 0, 0))

                self.upButton4.isOver(self.pos, (settings.width / 2), 0, (0, 200, 0), (0, 0, 0))
                self.downButton4.isOver(self.pos, (settings.width / 2), 0, (200, 0, 0), (0, 0, 0))

        """

            If a certain button is pressed it atomaticaly disables all other buttons
            that are not needed. This ensures interference with not displayed Buttons.

        """


        if pygame.mouse.get_pressed()[0] == 1:

            if self.herbSettingsState == False and self.carnSettingsState == False and self.foodSettingsState == False:  # if start Button is pressed buttons to change Values change

                if self.btn_start_stop.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    if settings.game_state == False:
                        settings.game_state = True
                        self.btn_start_stop.text = "STOP"
                    else:
                        settings.game_state = False
                        self.btn_start_stop.text = "START"

                if self.btn_reset.isOver(self.pos,settings.width/2,0 , (133,133,133),(0,0,0)):
                    settings.resetbuttonstate = True
                    settings.herbivore_state = True
                    settings.carnivore_state = True
                    self.btn_herb_state.text = "Herbivore OFF"
                    self.btn_bait_state.text = "Food OFF"
                    self.btn_carn_state.text = "Carnivore OFF"

                    settings.bait_state = True

                if self.btn_hideouts.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    if settings.hideout_state == False:
                        settings.hideout_state = True
                        self.btn_hideouts.text = "Hideouts OFF"
                        settings.msg_text = "hideouts turned on"

                    else:
                        settings.hideout_state = False
                        self.btn_hideouts.text = "Hideouts ON"
                        settings.msg_text = "hideouts turned off"

                if self.btn_carn_settings.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.carnSettingsState = True
                    self.game_state = False

                if self.btn_herb_settings.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.herbSettingsState = True
                    self.game_state = False

                if self.btn_bait_settings.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.foodSettingsState = True
                    self.game_state = False

                if self.btn_plot.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    settings.plot_id = (settings.plot_id+1) % 3
                    self.btn_plot.text = "Plot {:d}".format(settings.plot_id+1)

                if self.btn_csv_output.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    print("export values")
                    settings.savebuttonstate = True

                if self.btn_export_settings.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.export_settings()
                    print("successfully exported settings")
                    settings.msg_text = "successfully exported settings"

                if self.btn_import_settings.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    if self.import_settings():
                        print("successfully loaded settings")
                        settings.msg_text = "successfully loaded settings"
                    else:
                        print("can't load settings: file not accessible")
                        settings.msg_text = "file not accessible"

            #if the herbivoreSettingsState is Ture only the buttons for the herbivore settings are enabled

            if self.herbSettingsState == True:
                self.tb_variable_1.text = str(settings.am_herb)
                self.tb_variable_2.text = str(int(settings.ls_herb))
                self.tb_variable_3.text = str(round(settings.vel_herb*1000,1))
                self.tb_variable_4.text = str(settings.rep_herb)

                if self.btn_back.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.herbSettingsState = False
                    print("BACK")

                if self.upButton1.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.am_herb += 1
                    self.tb_variable_1.text = str(settings.am_herb)

                if self.downButton1.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.am_herb <= 0:
                        settings.am_herb = 0
                    else:
                        settings.am_herb -= 1
                    self.tb_variable_1.text = str(settings.am_herb)

                if self.upButton2.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.ls_herb += 10
                    self.tb_variable_2.text = str(int(settings.ls_herb))

                if self.downButton2.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.ls_herb <= 0:
                        settings.ls_herb = 0
                    else:
                        settings.ls_herb -= 10
                    self.tb_variable_2.text = str(int(settings.ls_herb))

                if self.upButton3.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.vel_herb += 0.0001
                    self.tb_variable_3.text = str(round(settings.vel_herb*1000,1))

                if self.downButton3.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.vel_herb <= 0:
                        settings.vel_herb = 0
                    else:
                        settings.vel_herb -= 0.0001
                    self.tb_variable_3.text = str(round(settings.vel_herb*1000,1))

                if self.upButton4.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.rep_herb += 1
                    self.tb_variable_4.text = str(settings.rep_herb)

                if self.downButton4.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.rep_herb <= 1:
                        settings.rep_herb = 1
                    else:
                        settings.rep_herb -= 1
                    self.tb_variable_4.text = str(settings.rep_herb)

                if self.btn_herb_state.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    if settings.herbivore_state == False:
                        settings.herbivore_state = True
                        self.btn_herb_state.text = "Herbivore OFF"
                    else:
                        settings.herbivore_state = False
                        self.btn_herb_state.text = "Herbivore ON"

            #if the carnivoreSettingsstate is True, only the buttons for the carnivore settings are enabled

            if self.carnSettingsState == True:
                self.tb_variable_1.text = str(settings.am_carn)
                self.tb_variable_2.text = str(int(settings.ls_carn))
                self.tb_variable_3.text = str(round(settings.vel_carn*1000,1))
                self.tb_variable_4.text = str(settings.rep_carn)

                if self.btn_back.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.carnSettingsState = False

                if self.upButton1.isOver(self.pos, settings.width / 2, 0, (0, 200, 0), (0, 0, 0)):
                    settings.am_carn += 1
                    self.tb_variable_1.text = str(settings.am_carn)

                if self.downButton1.isOver(self.pos, settings.width / 2, 0, (200, 0, 0), (0, 0, 0)):
                    if settings.am_carn <= 0:
                        settings.am_carn = 0
                    else:
                        settings.am_carn -= 1
                    self.tb_variable_1.text = str(settings.am_carn)

                if self.upButton2.isOver(self.pos, settings.width / 2, 0, (0, 200, 0), (0, 0, 0)):
                    settings.ls_carn += 10
                    self.tb_variable_2.text = str(int(settings.ls_carn))

                if self.downButton2.isOver(self.pos, settings.width / 2, 0, (200, 0, 0), (0, 0, 0)):
                    if settings.ls_carn <= 0:
                        settings.ls_carn = 0
                    else:
                        settings.ls_carn -= 10
                    self.tb_variable_2.text = str(int(settings.ls_carn))

                if self.upButton3.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.vel_carn += 0.0001
                    self.tb_variable_3.text = str(round(settings.vel_carn*1000,1))

                if self.downButton3.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.vel_carn <= 0:
                        settings.vel_carn = 0
                    else:
                        settings.vel_carn -= 0.0001
                    self.tb_variable_3.text = str(round(settings.vel_carn*1000,1))

                if self.upButton4.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.rep_carn += 1
                    self.tb_variable_4.text = str(settings.rep_carn)

                if self.downButton4.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.rep_carn <= 1:
                        settings.rep_carn = 1
                    else:
                        settings.rep_carn -= 1
                    self.tb_variable_4.text = str(settings.rep_carn)

                if self.btn_carn_state.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    if settings.carnivore_state == False:
                        settings.carnivore_state = True
                        self.btn_carn_state.text = "Carnivore OFF"
                    else:
                        settings.carnivore_state = False
                        self.btn_carn_state.text = "Carivore ON"


            #if foodSettingsState is true only the buttons for the food settings are enabled

            if self.foodSettingsState == True:
                self.tb_variable_1.text = str(settings.am_bait)
                self.tb_variable_2.text = str(int(settings.am_bait_max))
                self.tb_variable_3.text = str(round(settings.rep_bait*100,1))

                if self.btn_back.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    self.foodSettingsState = False

                if self.upButton1.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.am_bait += 1
                    self.tb_variable_1.text = str(settings.am_bait)

                if self.downButton1.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.am_bait <= 0:
                        settings.am_bait = 0
                    else:
                        settings.am_bait -= 1
                    self.tb_variable_1.text = str(settings.am_bait)

                if self.upButton2.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.am_bait_max += 1
                    self.tb_variable_2.text = str(settings.am_bait_max)

                if self.downButton2.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.am_bait_max <= 0:
                        settings.am_bait_max = 0
                    else:
                        settings.am_bait_max -= 1
                    self.tb_variable_2.text = str(settings.am_bait_max)

                if self.upButton3.isOver(self.pos,settings.width/2,0, (0,200,0),(0,0,0)):
                    settings.rep_bait += 0.01
                    self.tb_variable_3.text = str(round(settings.rep_bait*100,1))

                if self.downButton3.isOver(self.pos,settings.width/2,0, (200,0,0),(0,0,0)):
                    if settings.rep_bait <= 0:
                        settings.rep_bait = 0
                    else:
                        settings.rep_bait -= 0.01
                    self.tb_variable_3.text = str(round(settings.rep_bait*100,1))

                if self.btn_bait_state.isOver(self.pos,settings.width/2,0, (133,133,133),(0,0,0)):
                    if settings.bait_state == False:
                        settings.bait_state = True
                        self.btn_bait_state.text = "Food OFF"
                    else:
                        settings.bait_state = False
                        self.btn_bait_state.text = "Food ON"

        return self.size_changed



    """
        This method updates the plot and outputs new informations to the screen.
    """
    def update_plot(self, plot_screen):
        self.screen.blit(plot_screen, (settings.width/2,settings.height/3))

    """
        This method exports all the settings values in form of a exel sheet.
        This is especialy usefull for data analysis.

    """
    def export_settings(self):
        # convert to dict:
        tmp_dict1 = {
            "height"            : settings.height,
            "width"             : settings.width,

            "am_bait"           : settings.am_bait,
            "am_bait_max"       : settings.am_bait_max,
            "rep_bait"          : settings.rep_bait,

            "am_herb"           : settings.am_herb,
            "ls_herb"           : settings.ls_herb,
            "vel_herb"          : settings.vel_herb,
            "rep_herb"          : settings.rep_herb,

            "am_carn"           : settings.am_carn,
            "ls_carn"           : settings.ls_carn,
            "vel_carn"          : settings.vel_carn,
            "rep_carn"          : settings.rep_carn,

            "hideouts"          : settings.hideout_state,
            "am_hideout"        : settings.am_hideout,

            "game_state"        : settings.game_state,
            "plot_id"           : settings.plot_id,
            "resetbuttonstate"  : settings.resetbuttonstate
        }

        # export to '../' + self.settings_filename
        with open('./' + self.settings_filename, 'w') as csvfile:
            writer = csv.writer(csvfile, delimiter=';')
            for item in tmp_dict1:
                writer.writerow([str(item), str(tmp_dict1[item])])

    """

        This method imports settings that were exported before.
        This can be used in case that ideal settings have been
        exported before and want to be imported again. If there are no
        settings found there is a error message.

    """

    def import_settings(self):
        tmp_dict = {}
        try:
            # import '../' + self.settings_filename
            with open('./' + self.settings_filename, 'r') as csvfile:
                reader = csv.reader(csvfile, delimiter=';')

                for row in reader:
                    if len(row)>=2:
                        tmp_dict[row[0]] = row[1]


            settings.am_bait = int(tmp_dict['am_bait'])
            settings.am_bait_max = int(tmp_dict['am_bait_max'])
            settings.rep_bait = float(tmp_dict['rep_bait'])

            settings.am_herb = int(tmp_dict['am_herb'])
            settings.ls_herb = int(tmp_dict['ls_herb'])
            settings.vel_herb = float(tmp_dict['vel_herb'])
            settings.rep_herb = int(tmp_dict['rep_herb'])

            settings.am_carn = int(tmp_dict['am_carn'])
            settings.ls_carn = int(tmp_dict['ls_carn'])
            settings.vel_carn = float(tmp_dict['vel_carn'])
            settings.rep_carn = int(tmp_dict['rep_carn'])

            settings.hideout_state = bool_check(tmp_dict['hideouts'])
            settings.am_hideout = int(tmp_dict['am_hideout'])
            return True

        except IOError:
            return False
    """
        This function is called, when the screen is resized. To ensure that the buttons are plced
        at correct spot, relative to the screensize.
    """
    def resize_buttons(self):

        self.screen = pygame.display.set_mode((settings.width, settings.height), pygame.RESIZABLE)
        self.mainScreen = pygame.Surface((settings.width,settings.height))
        self.btn_start_stop.x = settings.width*8/200
        self.btn_reset.x = settings.width*38/200
        self.btn_hideouts.x = settings.width*68/200
        self.btn_carn_settings.x = settings.width*8/200
        self.btn_herb_settings.x = settings.width*38/200
        self.btn_bait_settings.x = settings.width*68/200
        self.btn_plot.x = settings.width*8/200
        self.btn_back.x = settings.width*8/200

        self.btn_csv_output.x = settings.width*38/200
        self.btn_export_settings.x = settings.width*8/200
        self.btn_import_settings.x = settings.width*38/200

        self.btn_carn_state.x = settings.width*38/200
        self.btn_herb_state.x = settings.width*38/200
        self.btn_bait_state.x = settings.width*38/200


        self.msg_box.x = settings.width*38/200-175

        self.upButton1.x = settings.width * 80 / 200-15
        self.downButton1.x = settings.width * 80 / 200+95
        self.upButton2.x = settings.width * 80 / 200-15
        self.downButton2.x = settings.width * 80 / 200+95
        self.upButton3.x = settings.width * 80 / 200-15
        self.downButton3.x = settings.width * 80 / 200+95
        self.upButton4.x = settings.width * 80 / 200-15
        self.downButton4.x = settings.width * 80 / 200+95
        self.tb_variable_1.x = settings.width * 80 / 200
        self.tb_variable_2.x = settings.width * 80 / 200
        self.tb_variable_3.x = settings.width * 80 / 200
        self.tb_variable_4.x = settings.width * 80 / 200
        self.tb_lbl_start_value.x = settings.width * 80/200-136
        self.lbl_start_value_u_1.x = settings.width * 80/200-136
        self.lbl_start_value_u_2.x = settings.width * 80/200-136
        self.lbl_lifespan.x = settings.width * 80/200-136
        self.lbl_lifespan_u_1.x = settings.width * 80/200-136
        self.lbl_velocity.x = settings.width * 80/200-136
        self.lbl_velocity_u_1.x = settings.width * 80/200-136
        self.lbl_reproduction.x = settings.width * 80/200-136
        self.lbl_reproduction_u_1.x = settings.width * 80/200-136
        self.lbl_max_amount.x = settings.width * 80/200-136
        self.lbl_max_amount_u_1.x = settings.width * 80/200-136
        self.lbl_regeneration.x = settings.width * 80/200-136
        self.lbl_regeneration_u_1.x = settings.width * 80/200-136
