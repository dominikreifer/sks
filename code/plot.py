import matplotlib, random
import math
import matplotlib.backends.backend_agg as agg
import pylab
import pygame
import csv, time
from pygame.locals import *
import settings
from profiler import profile



"""
    This class is used to plot the Herbivore, Carnivore and Bait values in realtime. 
    A time plot as well as phase portrait are possible.
    The basic function are initialization, resize, update of the values and update the plot.  
"""
class Plot:

    """
        Initialization of the plot. The size of the plot is set and receives its width and height from the assigned subsurface. 
        Therefore the width and height are changed to inches with dpi = 100.
        A canvas in a non-interactive mode is initialized and a renderer for the canvas is created. 
        The arrays for the animals, the bait and the time are set.
    """
    def __init__(self):
        self.fig = pylab.figure(figsize=[(settings.width/2)/100, (2*settings.height/3)/100]) 

        self.ax = self.fig.gca()

        self.canvas = agg.FigureCanvasAgg(self.fig)
        self.canvas.draw()
        self.renderer = self.canvas.get_renderer()
        self.raw_data = self.renderer.tostring_rgb()

        self.values1 = []
        self.values2 = []
        self.values3 = []
        self.times = []
        self.c = 0

        self.size = self.canvas.get_width_height()


    """
        With the resize function the size of the plot can be adjusted to the window size after the window size changed. 
        It includes the same functions as the init function described above.
    """
    def resize(self):
        self.fig = pylab.figure(figsize=[(settings.width / 2) / 100, (
                    2 * settings.height / 3) / 100])

        self.ax = self.fig.gca()

        self.canvas = agg.FigureCanvasAgg(self.fig)
        self.canvas.draw()
        self.renderer = self.canvas.get_renderer()
        self.raw_data = self.renderer.tostring_rgb()

        self.size = self.canvas.get_width_height()
        
    """
        As long as the simulation is active, the new values for the animals, the time and the food get attached to the arrays. 
        If the simulation is resetted, the values in the arrays are deleted and the values of the just recently started simulation are written to the arrays.
        It is possible to save the simulation data to a csv-file. The file will be saved in a folder named "results" and includes the data from the arrays. 
    """
    def update_values(self, val1, val2, val3):

        # update the arrays for animals, time and food
        if settings.game_state == True:
            self.times.append(self.c)
            self.values1.append([val1])
            self.values2.append([val2])
            self.values3.append([val3])

            self.c += 1

        # reset simulation and therefore clear the arrays
        if settings.resetbuttonstate == True:
            self.c = 0
            self.values1.clear()
            self.values2.clear()
            self.values3.clear()
            self.times.clear()

            self.times.append(self.c)
            self.values1.append([val1])
            self.values2.append([val2])
            self.values3.append([val3])

            settings.resetbuttonstate = False

        # export all data to .csv-file
        if settings.savebuttonstate == True:
            filename = '../results/' + 'output_{:s}.csv'.format(time.strftime("%Y_%m_%d_%H_%M_%S"))
            with open(filename, 'w') as csvfile:
                writer = csv.writer(csvfile, delimiter=';')
                writer.writerow(["tick","carnivore","food","herbivore"])
                for i in range(len(self.times)):
                    writer.writerow([self.times[i],self.values1[i][0],self.values2[i][0],self.values3[i][0]])

            print("successfully exported values to file", filename)
            settings.msg_text = "successfully saved values"
            settings.savebuttonstate = False


    """
        The user can switch between three different plots to show the simulated data.
        The first possibility - as a default setting at the beginning of each simulation - shows the growth of the populations and the food plotted against the simulation time.
        The range of time always shows the last 5000 ticks. In this manner the plot can be animated and cannot distort the curves.
        Only the values for the displayed animals and the displayed food will be plotted (due to the possibility that the user can decide whether or not to simulate the whole environment).
        The second and third possible plots are phase portraits that show the relation between the one that gets eaten and the one that does eat.
    """
    def update_plot(self):
        # time plot
        if settings.plot_id == 0:
            if len(self.times) <= 5000:
                self.ax.set_xlim(-1, 5000)
            else:
                self.ax.set_xlim(self.times[-5000], self.times[-1])
            if len(self.times) <= 1:
                self.ax.set_ylim(-1,210)
            else:
                if max((max(self.values1[-5000:-1]), max(self.values2[-5000:-1]), max(self.values3[-5000:-1])))[0]<=200:
                    self.ax.set_ylim(-1, 210)
                else:
                    self.ax.set_ylim(-1,max((max(self.values1[-5000:-1]), max(self.values2[-5000:-1]), max(self.values3[-5000:-1])))[0]+10)

            if settings.carnivore_state:
                self.ax.plot(self.times[-5000:],self.values1[-5000:], '#ff0000', label='Carnivore')
            if settings.herbivore_state:
                self.ax.plot(self.times[-5000:],self.values3[-5000:], '#00ff00', label='Herbivore')
            if settings.bait_state: 
                self.ax.plot(self.times[-5000:],self.values2[-5000:], '#ffbe40', label='Food')
            
            # settings for time plot
            self.ax.legend(loc='upper right')
            self.ax.grid(True)
            self.ax.set_xlabel('Time')
            self.ax.set_ylabel('Population')
            self.ax.set_title('Population over Time')
        
        # phase portrait: herbivore <-> carnivore
        elif settings.plot_id == 1:
            if len(self.times) <=1:
                self.ax.set_xlim(-1, 155)
                self.ax.set_ylim(-1, 65)
            else:
                if max(self.values3)[0]<= 150:
                    self.ax.set_xlim(-1,155)
                else:
                    self.ax.set_xlim(-1,max(self.values3)[0]+5)
                if max(self.values1)[0]<= 60:
                    self.ax.set_ylim(-1,65)
                else:
                    self.ax.set_ylim(-1,max(self.values1)[0]+5)
            
            # settings for phase portrait herbivore <-> carnivore
            self.ax.plot(self.values3, self.values1)
            self.ax.set_xlabel('Herbivore')
            self.ax.set_ylabel('Carnivore')
            self.ax.grid(True)
            self.ax.set_title('Phase Portrait Carnivore over Herbivore')
        
        # phase portrait: herbivore <-> food
        elif settings.plot_id == 2:
            if len(self.times) <=1:
                self.ax.set_xlim(-1, 205)
                self.ax.set_ylim(-1, 155)
            else:
                if max(self.values2)[0]<= 200:
                    self.ax.set_xlim(-1,205)
                else:
                    self.ax.set_xlim(-1,max(self.values2)[0]+5)
                if max(self.values3)[0]<= 150:
                    self.ax.set_ylim(-1,155)
                else:
                    self.ax.set_ylim(-1,max(self.values3)[0]+5)

            # settings for phase portrait herbivore <-> food
            self.ax.plot(self.values2, self.values3)
            self.ax.set_xlabel('Food')
            self.ax.set_ylabel('Herbivore')
            self.ax.grid(True)
            self.ax.set_title('Phase Portrait Herbivore over Food')


        self.canvas = agg.FigureCanvasAgg(self.fig)
        self.canvas.draw()
        self.renderer = self.canvas.get_renderer()
        self.raw_data = self.renderer.tostring_rgb()

        self.ax.cla()

        return pygame.image.fromstring(self.raw_data, self.size, "RGB")
