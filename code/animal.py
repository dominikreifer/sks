import pygame
import random
import settings
from mathematics import *
from profiler import profile

"""
This class serves as parent class for all animals.
The basic functions are initialization, die, attract and avoid walls
"""
class animal(object):

    """
    Initialization of an animal. The ID of the animal is set, it gives each animal a unique number which is used to address the animal.
    The starting position and direction are set randomly as well as an offset for the lifespan and velocity.
    """
    def __init__(self, id):
        self.id = id

        self.offset_lifespan = random.randint(-200,200)
        self.offset_vel = random.uniform(-0.0001, 0.0001)

        self.pos = v2(random.uniform(0.003,0.997), random.uniform(0.003,0.997))

        self.dir = v2(random.uniform(-2,2),random.uniform(-2,2))
        self.dir.norm()

        self.energy = 0

    """
    This function is called when the lifespan of the animal is exceeded and when it is eaten by another animal.
    It removes the animal from the list and updates the IDs of the remaining animals.
    """
    def die(self, list):
        list.pop(self.id)
        new_id = 0
        for item in list:
            item.id =new_id
            new_id += 1

    """
    Returns the vector pointing to the point given as parameter.
    """
    def attract(self, v_pos):
        return v2(-( self.pos.x - v_pos.x), -(self.pos.y - v_pos.y)).get_norm()

    """
    The animal avoids the borders of the simulated area. The animal receives a negative attraction vector.
    """
    def avoid_walls(self, weighting):
        border = 0.002
        # walls
        v_walls = v2(0,0)
        # left
        v_walls -= self.attract(v2(border, self.pos.y)) * (1/distance(self.pos.x, self.pos.y, border, self.pos.y))
        # right
        v_walls -= self.attract(v2( 1-border, self.pos.y)) * (1/distance(self.pos.x, self.pos.y,  1-border, self.pos.y))
        # top
        v_walls -= self.attract(v2(self.pos.x, border)) * (1/distance(self.pos.x, self.pos.y, self.pos.x, border))
        # bottom
        v_walls -= self.attract(v2(self.pos.x,  1-border)) * (1/distance(self.pos.x, self.pos.y, self.pos.x,  1-border))

        self.dir += v_walls*weighting


"""
Sub class of the animal class. It is used to create an herbivore. The properties of the animal class are inherited and
specific methods and attributes are added.
"""
class herbivore(animal):

    """
    Initialization of the herbivore. The init function of the parent class is called, the lifespan and velocity are set
    from the settings.py with a random offset.
    """
    def __init__(self,id):
        animal.__init__(self,id)
        self.lifespan = int(settings.ls_herb) + self.offset_lifespan
        self.lifespan_left = self.lifespan
        self.vel = settings.vel_herb + self.offset_vel

    """
    Method to eat a bait object from the food array. The energy index is increased by one.
    """
    def eat(self, list, id):
        list[id].die(list)
        self.energy += 1

    """
    Method to spawn a new herbivore. The function is called when the energy level is large enough for reproduction.
    The new herbivore is added to the list of herbivores and the starting position is set close to the position of the parent.
    """
    def spawn(self, list):
        list.append(herbivore(len(list)))
        list[-1].pos.x = self.pos.x + random.uniform(-0.01, 0.01)
        list[-1].pos.y = self.pos.y + random.uniform(-0.01, 0.01)
        self.energy -= settings.rep_herb

    """
    The herbivore is searching for food. The distance to all bait objects is calculated. The herbivore is attracted by
    bait object, if the distance is small enough.
    """
    def search(self, l_food, weighting):
        attr_id = 0
        min_dist = (settings.width/2)*settings.height
        for bait in l_food:
            tmp = distance(self.pos.x, self.pos.y, bait.pos.x, bait.pos.y)
            if tmp < min_dist:
                min_dist = tmp
                attr_id = bait.id

        # if distance small enough, chase food
        if 0.05 > min_dist > 0.0075:
            self.dir += self.attract(l_food[attr_id].pos)*weighting

        # remove food
        if min_dist <= 0.0075:
            self.eat(l_food,attr_id)

    """
    The herbivore is drawn to the screen of the simulation. The herbivore is made up of a circle and a straight line.
    """
    def draw(self, screen):
        for i in range(1):
            pygame.draw.circle(screen, (0,255,0), (int(self.pos.x*(settings.width/2)), int(self.pos.y*settings.height)), 3)
            pygame.draw.line(screen, (0,255,0), (int(self.pos.x*(settings.width/2)), int(self.pos.y*settings.height)), (self.pos.x*(settings.width/2) + 10*self.dir.x, self.pos.y*settings.height + 10*self.dir.y), 1)

    """
    Method to update the herbivore. It is checked whether changes to the settings.py have been made. New values from the
    settings are applied. The remaining lifespan is decreased by one, a new herbivore is spawned if the energy level is
    high enough and the new position of the animal is calculated.
    """
    def update(self,l_animal, l_food):
        if settings.vel_herb != self.vel-self.offset_vel:
            self.vel = settings.vel_herb+self.offset_vel

        if settings.ls_herb != self.lifespan-self.offset_lifespan:
            self.lifespan_left = self.lifespan_left+(settings.ls_herb-self.lifespan)
            self.lifespan = settings.ls_herb+self.offset_lifespan

        # calculate lifespan and check for reproduction/death
        self.lifespan_left -= 1
        if self.lifespan_left <= 0:
            self.die(l_animal)

        if self.energy >= settings.rep_herb:
            self.spawn(l_animal)

        # add vectors for movement
        self.avoid_walls(0.001)

        if settings.bait_state:
            self.search(l_food, 0.25)

        self.dir.norm()
        self.pos = self.pos + self.dir*self.vel

        # prevents that the animal is leaving the simulated area
        if self.pos.x < 0.002:
            self.pos.x = 0.003
        if self.pos.x > 0.998:
            self.pos.x = 0.997
        if self.pos.y < 0.002:
            self.pos.y = 0.003
        if self.pos.y > 0.998:
            self.pos.y = 0.997


"""
Sub class of the animal class. It is used to create an carnivore. The properties of the animal class are inherited and
specific methods and attributes are added.
"""
class carnivore(animal):

    """
    Initialization of the carnivore. The init function of the parent class is called, the lifespan and velocity are set
    from the settings.py with a random offset.
    """
    def __init__(self,id):
        animal.__init__(self,id)
        self.lifespan = settings.ls_carn + self.offset_lifespan
        self.lifespan_left = self.lifespan
        self.vel = settings.vel_carn + self.offset_vel

    """
    Method to eat a herbivore object from the food array. The energy index is increased by one.
    """
    def eat(self, list, id):
        list[id].die(list)
        self.energy += 1

    """
    Method to spawn a new carnivore. The function is called when the energy level is large enough for reproduction.
    The new carnivore is added to the list of carnivores and the starting position is set close to the position of the parent.
    """
    def spawn(self, list):
        list.append(carnivore(len(list)))
        list[-1].pos.x = self.pos.x+random.uniform(-0.01,0.01)
        list[-1].pos.y = self.pos.y+random.uniform(-0.01,0.01)
        self.energy -= settings.rep_carn

    """
    The herbivore is searching for food. The distance to all bait objects is calculated. The herbivore is attracted by
    bait object, if the distance is small enough.
    """
    def search(self, l_food, weighting):
        attr_id = 0
        min_dist = (settings.width/2)*settings.height
        for animal in l_food:
            tmp = distance(self.pos.x, self.pos.y, animal.pos.x, animal.pos.y)
            if tmp < min_dist:
                min_dist = tmp
                attr_id = animal.id

        # if distance small enough, chase herbivore
        if 0.1 > min_dist > 0.0075:
            self.dir += self.attract(l_food[attr_id].pos)*weighting
            l_food[attr_id].dir += self.attract(l_food[attr_id].pos) * (weighting * 1)
            l_food[attr_id].dir.norm()

        # remove herbivore
        if min_dist <= 0.0075:
            self.eat(l_food, attr_id)

    """
    The carnivore is avoiding hideouts when they are turned on. The carnivore receives a negative attraction vector.
    """
    def avoid_hideouts(self, l_hideout, weighting):
        avoid_id = 0
        min_dist = l_hideout[0].size
        for hideout in l_hideout:
            tmp = distance(self.pos.x, self.pos.y, hideout.pos.x, hideout.pos.y)
            if tmp < min_dist:
                min_dist = tmp
                avoid_id = hideout.id

        # if too close, turn away
        avg_dis = l_hideout[avoid_id].size/40
        if 0.1*avg_dis > min_dist > 0.0075*avg_dis:
            self.dir -= self.attract(l_hideout[avoid_id].pos)*weighting

    """
    The carnivore is drawn to the screen of the simulation. The carnivore is made up of two circles and a straight line.
    """
    def draw(self, screen):
        for i in range(1):
            pygame.draw.circle(screen, (255,0,0), (int(self.pos.x*(settings.width/2)), int(self.pos.y*settings.height)), 3)
            pygame.draw.circle(screen, (255,0,0), (int(self.pos.x*(settings.width/2)+self.dir.x*5), int(self.pos.y*settings.height+self.dir.y*5)), 3)
            pygame.draw.line(screen, (255,0,0), (int(self.pos.x*(settings.width/2)), int(self.pos.y*settings.height)), (self.pos.x*(settings.width/2) + 10*self.dir.x, self.pos.y*settings.height + 10*self.dir.y), 1)

    """
    Method to update the carnivore. It is checked whether changes to the settings.py have been made. New values from the
    settings are applied. The remaining lifespan is decreased by one, a new carnivore is spawned if the energy level is
    high enough and the new position of the animal is calculated.
    """
    def update(self, l_animal, l_food, l_hideout):
        if settings.vel_carn != self.vel-self.offset_vel:
            self.vel = settings.vel_carn+self.offset_vel

        if settings.ls_carn != self.lifespan-self.offset_lifespan:
            self.lifespan_left = self.lifespan_left+(settings.ls_carn-self.lifespan)
            self.lifespan = settings.ls_herb+self.offset_lifespan

        # calculate lifespan and check for reproduction/death
        self.lifespan_left -= 1
        if self.lifespan_left <= 0:
            self.die(l_animal)

        if self.energy >= settings.rep_carn:
            self.spawn(l_animal)

        # add vectors for movement
        self.avoid_walls(0.001)
        if settings.herbivore_state:
            self.search(l_food, 0.15)
        if settings.hideout_state:
            self.avoid_hideouts(l_hideout, 0.4)

        self.dir.norm()
        self.pos = self.pos + self.dir*self.vel

        # prevents that the animal is leaving the simulated area
        if self.pos.x < 0.002:
            self.pos.x = 0.003
        if self.pos.x > 0.998:
            self.pos.x = 0.997
        if self.pos.y < 0.002:
            self.pos.y = 0.003
        if self.pos.y > 0.998:
            self.pos.y = 0.997
