import pygame
from pygame import gfxdraw
import os
"""
    This class creates a Button to enable user interaction. The Class has two functions drawing the button
    and checking if the mouseposition coincides with the position of the mouse

    This class is based on a written code from https://www.youtube.com/watch?v=4_9twnEduFA. Varriations were made
    for our application
"""

class button():

    """
        Initialization of all the attributes of the button
        *color - backgroundcolor of the button
        *x - x-coordinates of top left corner of button
        *y - y-coordinates of top left corner of the button
        *width and hight - dimentions of the poy in Pixels
        *text and text_color- text that is displayed in the textbox and the color of the text
        *font - font of the text is set to default
    """
    def __init__(self, color, x,y,width,height, text=''):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.text_color = (255,255,255)
        self.font = pygame.font.Font(os.getcwd() +'/src/DejaVuSans.ttf', 12)

    """
        Draws the button with the set attributes and senters the text in the Button
    """
    def draw(self,screen,outline=None):
        #Call this method to draw the button on the screen
        if outline:
            pygame.draw.rect(screen, outline, (self.x-2,self.y-2,self.width+4,self.height+4),0)

        pygame.draw.rect(screen, self.color, (self.x,self.y,self.width,self.height),0)

        if self.text != '':
            #font = pygame.font.SysFont('arialblack', 20)
            text = self.font.render(self.text, 1, self.text_color)
            screen.blit(text, (self.x + (self.width/2 - text.get_width()/2), self.y + (self.height/2 - text.get_height()/2))) #center Text in Button


    """
        Checks if the position of the mouse (pos) coincies with the position of the Button
        The onColor sets the color if the mouse if over the button and offColor sets the color if 
        the mouseposition does not coincide. 
        The xoffset and yoffset are needed because the mouseposition of can only refer to the main scren 
        and not to the subscreens. However the Buttons that are drawn in the Subscreen and relate their coordinates to the subscree. 
    """

    def isOver(self, pos, xoffset, yoffset, onColor, offColor):
        #Pos is the mouse position or a tuple of (x,y) coordinates
        if pos[0] > (self.x + xoffset) and pos[0] < (self.x + xoffset) + self.width:
            if pos[1] > (self.y + yoffset) and pos[1] < (self.y + yoffset) + self.height:
                self.color = onColor
                return True
            else:
                self.color = offColor

        else:
            self.color = offColor

        return False


"""
    This Class is used fot textboxes to seperate them from buttons. The attributes are the same. 
    However this texbox does not change color if the mouseposition coincodes with the textbox position
"""
class rectangle_text():
    def __init__(self, color, x,y,width,height, text='',text_color = (255,255,255)):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.text_color = text_color
        self.font = pygame.font.Font(os.getcwd() +'/src/DejaVuSans.ttf', 12)

    """
        Draws the textbox on the screen
    """

    def draw(self,screen,outline=None):
        #Call this method to draw the button on the screen
        if outline:
            pygame.draw.rect(screen, outline, (self.x-1,self.y-1,self.width+2,self.height+2),0)

        pygame.draw.rect(screen, self.color, (self.x,self.y,self.width,self.height),0)

        if self.text != '':
            text = self.font.render(self.text, 1, self.text_color)
            screen.blit(text, (self.x + (self.width/2 - text.get_width()/2), self.y + (self.height/2 - text.get_height()/2))) #center Text in Button

    """
        Checks if the button position coincides with the position of the mouse that is handed over with the pos variable.
        The xoffset and y offset are needed because the mouseposition of can only refer to the main scren 
        and not to the subscreens. However the Buttons that are drawn in the Subscreen and relate their coordinates to the subscree.
    """
    def isOver(self, pos, xoffset, yoffset):
        #Pos is the mouse position or a tuple of (x,y) coordinates
        if pos[0] > (self.x + xoffset) and pos[0] < (self.x + xoffset) + self.width:
            if pos[1] > (self.y + yoffset) and pos[1] < (self.y + yoffset) + self.height:
                return True

        return False


"""
    This class was used to draw circular buttons but is not used. However if needed it can be used. 
"""

"""
class circular_button():
    def __init__(self, color, center, r, text=''):
        self.color = color
        self.center = (int(center[0]), int(center[1]))  #center is a tuple that contains x,y koordinates
        self.r = r
        self.text = text


    def draw(self,screen,outline=None):
        #Call this method to draw the button on the screen
        if outline:
           pygame.draw.circle(screen, outline, self.center,self.r+2, 0)

        pygame.draw.circle(screen, self.color, self.center, self.r)

        if self.text != '':
            text = self.font.render(self.text, 1, self.text_color)
            screen.blit(text, (self.center[0]-text.get_width()/2,self.center[1]-text.get_height()/2)) #center Text in Button


    def isOver(self, pos, xoffset, yoffset, onColor, offColor):
        #Pos is the mouse position or a tuple of (x,y) coordinates
        if pos[0] > ((self.center[0]-self.r)+xoffset) and pos[0] < ((self.center[0]+ self.r) + xoffset):
            if pos[1] > self.center[1]-self.r and pos[1] < self.center[1] + self.r:
                self.color = onColor
                return True
            else:
                self.color = offColor
        else:
            self.color = offColor

        return False
"""

"""
    This class is used to draw the triangle arrows to varry values during the simulation. They work the same way 
    as the Button Class.

"""
class Triangle_Button():

    """
        Initializing the Objects with attributes.
    """
    def __init__(self, color, x, y, height, width, line, orientation):
        self.color = color
        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.line = line
        self.orientation = orientation #gives the orientation of the triangel, facing up or down, depending on if values should be increased or decreased
    """
        Draws the Triangles depending on their initialized attributes. Depending on the orientation variable, 
        the Triangle orientation is altered therefore drawn differently
    """

    def draw(self, screen):
        if(self.orientation == 1):
            pygame.gfxdraw.aapolygon(screen, (((self.x),(self.y)), ((self.x-self.width/2),(self.y+self.height)), ((self.x + self.width/2),(self.y+self.height))), self.color)
            pygame.gfxdraw.filled_polygon(screen, (((self.x),(self.y)), ((self.x-self.width/2),(self.y+self.height)), ((self.x + self.width/2),(self.y+self.height))), self.color)
        else:
            pygame.gfxdraw.aapolygon(screen, (((self.x),(self.y)), ((self.x-self.width/2),(self.y-self.height)), ((self.x + self.width/2),(self.y-self.height))), self.color)
            pygame.gfxdraw.filled_polygon(screen, (((self.x),(self.y)), ((self.x-self.width/2),(self.y-self.height)), ((self.x + self.width/2),(self.y-self.height))), self.color)

    """
        Checks if the button position coincides with the position of the mouse that is handed over with the pos variable. The Color for when the mouseposition
        coinsides and does not coincide can be set by the user. 
        The xoffset and yoffset are needed because the mouseposition of can only refer to the main scren 
        and not to the subscreens. However the Buttons that are drawn in the Subscreen and relate their coordinates to the subscree.
    """
    def isOver(self, pos, xoffset, yoffset, onColor, offColor):
        #depending on the orientation of the Triangle the position check has to be done differently
        if(self.orientation == 1):
            if pos[0] > ((self.x-self.width/2) + xoffset) and pos[0] < ((self.x + self.width/2)+xoffset):# or pos[0] > (self.x-self.width/2 + xoffset) and pos[0] < (self.x+self.width/2+xoffset):
                if pos[1] > ((self.y)+yoffset) and pos[1] < ((self.y+self.height)+yoffset):# or pos[1] > ((self.y-self.height)+yoffset) and pos[1] < (self.y+yoffset):
                    self.color = onColor
                    return True
                else:
                    self.color = offColor
            else:
                self.color = offColor

                return False

        else:

            if pos[0] > (self.x-self.width/2 + xoffset) and pos[0] < (self.x+self.width/2+xoffset):
                if pos[1] > ((self.y-self.height)+yoffset) and pos[1] < (self.y+yoffset):
                    self.color = onColor
                    return True
                else:
                    self.color = offColor
            else:
                self.color = offColor

                return False
