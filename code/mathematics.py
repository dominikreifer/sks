import time, math

PI = 3.14159265359


"""
This function returns the distance as absolut value from two points.
"""
def distance(x1, y1, x2, y2):
     return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)


"""
This function returns if the given string is a boolean formatted as "True/true" or "False/false".
Python handles string containing a symbol automatically as True even if the string contains a "False".
"""
def bool_check(tmp_str):
    if 'True' in tmp_str or 'True' in tmp_str or '1' in tmp_str:
        return 1
    else:
        return 0

"""
Numpy or other libraries are offering vector classes but for reducing the overhead this v2-class creates a two dimensional vector element.
"""
class v2:

    """
    Initializing the vector with an x and y value.
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y



    """
    Method for adding two objects of the v2-class or adding a scalar value to each element of the v2-instance.
    Therefore a typecheck for v2 or integer is necessary.
    """
    def __add__(self, b):
        if type(b) == type(self):
            return v2(self.x + b.x, self.y + b.y)
        elif type(b) is (int) or type(b) is (float):
            return v2(self.x + b, self.y + b)
        else:
            raise TypeError("is not a number or a v2 object")

    """
    Method for substracting two objects of the v2-class or substracting a scalar value to each element of the v2-instance.
    Therefore a typecheck for v2 or integer is necessary.
    """
    def __sub__(self, b):
        if type(b) == type(self):
            return v2(self.x - b.x, self.y - b.y)
        elif type(b) is (int) or type(b) is (float):
            return v2(self.x - b, self.y - b)
        else:
            raise TypeError("is not a number or a v2 object")


    """
    Method for multiplying two objects of the v2-class or multiplying a scalar value to each element of the v2-instance.
    Therefore a typecheck for v2 or integer is necessary.
    """
    def __mul__(self, b):
        if type(b) == type(self):
            return v2(self.x * b.x + self.y * b.y)
        elif type(b) is (int) or type(b) is (float):
            return v2(self.x * b, self.y * b)
        else:
            raise TypeError("is not a number or a v2 object")


    """
    Set the vector to its euclidic norm of itself.
    """
    def norm(self):
        length = self.get_length()
        self.x = self.x / length
        self.y = self.y / length


    """
    Return the euclidic norm of the vector but doesn't change the value.
    """
    def get_norm(self):
        length = self.get_length()
        return v2(self.x / length, self.y / length)


    """
    Returns the angle phi of the vector.
    """
    def get_phi(self):
        if self.x < 0 and self.y < 0:
            return math.atan(self.y/self.x) - math.pi
        elif self.x == 0 and self.y < 0:
            return -math.pi/2
        elif self.x > 0:
            return math.atan(self.y/self.x)
        elif self.x == 0 and self.y > 0:
            return math.pi/2
        elif self.x < 0 and self.y >= 0:
            return math.atan(self.y/self.x) + math.pi

    """
    Adding the angle phi to the vector direction.
    """
    def add_phi(self, phi):
        length = self.get_length()
        self.x = math.cos(self.get_phi() + phi)
        self.y = math.sin(self.get_phi() + phi)
        self.norm()
        self = self*length


    """
    This method return the length of the vector as scalar.
    """
    def get_length(self):
        return math.sqrt(self.x**2 + self.y**2)
