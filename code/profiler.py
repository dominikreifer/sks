import time

_profiled_functions = {}
_profiled_loops = {}


"""
This function sets up a wrapper which takes a function as input parameter.
Before executing the function a timestamp is saved.
The difference between the starting and endpoint will be added  to the _profiled_functions-dictionary with the function name.
"""
def profile(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        ret = func(*args, **kwargs)
        stop = time.time()
        #print("[{:s}]: {:f}ms".format(func.__name__, (stop-start)*1000))
        _profiled_functions[func.__qualname__] = stop-start
        return ret
    return wrapper


"""
This function sets up a wrapper which takes a loop name as input parameter.
It stores the calling-timestamp with the loop name into _profiled_loops.
After calling it the second time with the same loop name, the time difference will be calculated and written to the _profiled_loops dictionary.
"""
def profile_loop(loop_name):
    calltime = time.time()
    if loop_name not in _profiled_loops:
        _profiled_loops[loop_name] = {'last': calltime, 'total': 0}
    _profiled_loops[loop_name]['total'] = calltime - _profiled_loops[loop_name]['last']
    _profiled_loops[loop_name]['last'] = calltime


"""
Printing all items from the two dictionaries.
"""
def print_stats():
    for item in _profiled_functions:
        print("[func][{:s}]: {:f}ms".format(item, _profiled_functions[item]*1000))
    for item in _profiled_loops:
        print("[loop][{:s}]: {:f}ms".format(item, _profiled_loops[item]['total']*1000))
