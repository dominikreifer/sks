import pygame
import random
import settings
from mathematics import *
from profiler import profile

"""
    This class is used to create the the hideouts for the Herbivores. 
    The basic functions are initialization and draw
    
"""

class hideout(object):

    """
        When Initialied, the hideouts are given a unique ID. The position and size of the
        hideouts are set randomly with in a selected interval
    """

    def __init__(self, id):
        self.id = id
        self.pos = v2(random.uniform(0.003,0.997), random.uniform(0.003,0.997))

        self.size = random.randint(20,35)



    """
    Draws the circle in a defined color, to simulate the hideouts for the Herbivores. 
    """
    def draw(self, screen):
        pygame.draw.circle(screen, (50,50,0), (int(self.pos.x*(settings.width/2)), int(self.pos.y*settings.height)), self.size)
