"""
This file contains parameters which will be used by different funtions and methods of classes of the project.
The values will be stored even if the program is not running.
"""

# display
height = int(765)
width = int(height*16/9)

# starting amount of animals and food
bait_state = True
am_bait = 150
am_bait_max = 200
rep_bait = 0.02

# Parameter Herbivore
herbivore_state = True
am_herb = 80
ls_herb = 1000
vel_herb = 0.0023
rep_herb = 3

# Parameter Carnivore
carnivore_state = True
am_carn = 40
ls_carn = 1000
vel_carn = 0.0027
rep_carn = 4

# Parameter hideout
hideout_state = False
am_hideout = 20

# global
game_state = False
fps_state = False
plot_id = 0
resetbuttonstate = False
savebuttonstate = False
msg_text = "WELCOME"
color = (210,210,210)
