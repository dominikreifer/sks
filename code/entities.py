from animal import *
from food import *
from hideout import *
import settings
from profiler import profile

"""
This class serves as manager for the different species and hideouts.
Those are the entities that have to be updated at every tick of the simulation.
It contains the arrays for the animals, hideouts and baits.
"""
class Entities:


    """
    Initialization of the array for: herbivores, carnivores, bait and hideouts.
    This includes getting the maximum amount from settings.py
    and filling an array with basic objects of the specific class.
    """
    def __init__(self):
        # create arrays for animals
        if settings.herbivore_state:
            self.l_herb = []
            for i in range(settings.am_herb):
                self.l_herb.append(herbivore(i))

        if settings.carnivore_state:
            self.l_carn = []
            for i in range(settings.am_carn):
                self.l_carn.append(carnivore(i))

        # create array for food
        if settings.bait_state:
            self.l_food = []
            for i in range(settings.am_bait):
                self.l_food.append(bait(i))

        #create array for hideout
        self.l_hideout = []
        for i in range(settings.am_hideout):
            self.l_hideout.append(hideout(i))


    """
    Method of updating the entities. It's called at every tick of the simulation.
    Each cycle includes a check for the active state of the entitiy, calling the update method of the species and draw the object on the screen.
    if the resetbutton at the interface is pressed, the arrays will be initialized again.
    """
    #@profile
    def update(self, surface):
        if settings.game_state:
            # update food
            if settings.bait_state:
                for item in self.l_food:
                    item.spawn(self.l_food)

            # update animals
            if settings.herbivore_state:
                for item in self.l_herb:
                    item.update(self.l_herb, self.l_food)

            if settings.carnivore_state:
                for item in self.l_carn:
                    item.update(self.l_carn, self.l_herb, self.l_hideout)

        if settings.resetbuttonstate == True:
            self.l_herb.clear()
            self.l_carn.clear()
            self.l_food.clear()
            if settings.hideout_state:
                self.l_hideout.clear()

            if settings.herbivore_state:
                for i in range(settings.am_herb):
                    self.l_herb.append(herbivore(i))

            if settings.carnivore_state:
                for i in range(settings.am_carn):
                    self.l_carn.append(carnivore(i))

            if settings.bait_state:
                for i in range(settings.am_bait):
                    self.l_food.append(bait(i))

            if settings.hideout_state:
                for i in range(settings.am_hideout):
                    self.l_hideout.append(hideout(i))

        #draw hideout
        if settings.hideout_state:
            for item in self.l_hideout:
                item.draw(surface)

        # draw food
        if settings.bait_state:
            for item in self.l_food:
                item.draw(surface)

        # draw animals
        if settings.herbivore_state:
            for item in self.l_herb:
                item.draw(surface)

        if settings.carnivore_state:
            for item in self.l_carn:
                item.draw(surface)
