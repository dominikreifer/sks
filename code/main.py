from interface import *
from plot import *
from entities import *
import profiler

"""
This is were all the magic happens. The main function initializes all the entities that are needed in the first part.
The second part consists of a loop (when variable running is True), that executes the main program
"""


def main():
    # initializing the pygame instance and changing the icon to the IDS logo
    pygame.init()
    pygame.display.set_caption('Predator-Prey-Simulation')
    program_icon = pygame.image.load('src/Logo IDS.png')
    pygame.display.set_icon(program_icon)

    # initializing the interface, plot and entities instance
    interface = Interface()
    plot = Plot()
    entities = Entities()

    # setting a bool variable, program is running while True
    running = True

    # refreshing the plot and all surfaces
    interface.refresh_surfaces()
    plot.update_values(len(entities.l_carn), len(entities.l_food), len(entities.l_herb))
    interface.update_plot(plot.update_plot())
    interface.update_surfaces()
    interface.refresh_surfaces()

    # program loop
    while running:

        # checking for pygame events, resize plot when the window is resized
        for event in pygame.event.get():
            size_changed = interface.check_events(event)
            if size_changed:
                plot.resize()

        # refreshing all surfaces and buttons
        interface.refresh_surfaces()
        interface.refresh_buttons()

        # calculate animals and food for the next time step
        entities.update(interface.subSurface1)

        # update plot
        plot.update_values(len(entities.l_carn), len(entities.l_food), len(entities.l_herb))
        if pygame.time.get_ticks()%15 == 0:
            interface.update_plot(plot.update_plot())
            # print profiler status if activated
            profiler.print_stats()

        # update all surfaces
        interface.update_surfaces()

# main function is called
if __name__ == "__main__":
    main()
