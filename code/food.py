import pygame
import random
import settings
from mathematics import *

"""
This class is used to create the the bait for the herbivores. 
The basic functions are initialization, draw, die and spawn
"""
class bait(object):

    """
        Initialization of the bait gives each bait object a  unique id, which is important for simulation the
        eating or dying of the bait. And a position that is set randomly.
    """

    def __init__(self, id):

        self.id = id

        self.pos = v2(random.uniform(0.003,0.997), random.uniform(0.003,0.997))

    """
    Draws the circle in a defined color, to simulate the bait. 
    """
    def draw(self, screen):
        pygame.draw.circle(screen, (255,190,64), (int(self.pos.x*(settings.width/2)), int(self.pos.y*settings.height)), 3)
  
    """
    This function is called when a herbivore eats a bait object. It removes the bait from the list and updates the ID
    remaining objects.
    """
    def die(self, list):
        list.pop(self.id)
        new_id = 0
        for item in list:
            item.id = new_id
            new_id += 1
    """
    Creates new bait entities during runtime. The position is set randomly.
    """
    def spawn(self, list):
        if len(list) < settings.am_bait_max:
            if random.random() < (1-len(list)/settings.am_bait_max)*settings.rep_bait:
                list.append(bait(len(list)))
